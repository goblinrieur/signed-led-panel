# signed LED panel

Here is a simple display of wide 7 segments LED's display (sign+2digits with decimal points) 
sign + digits will be made of RGB leds 
At first Inspiration comes from the https://hackaday.io/project/185748-7-segment-neopixel-display-module  idea


![FACE](./pics/diy_7seg_RGBpanel_back.png)

![BACK](./pics/diy_7seg_RGBpanel_face.png)

## Getting started

Will be drawn for schematic to production files using Kicad 6.x

```
#        ___   ___   _ 
#__   __/ _ \ / _ \ / |
#\ \ / / | | | | | || |
# \ V /| |_| | |_| || |
#  \_/  \___(_)___(_)_|
#                      
```

## License

see [unlicensed file](./LICENSE)

# Goal

The panel has to be usable on many projects so I must keep it simple & universal enough so :

- [X] use standard RGB leds  NEOPIXEL : ws2812b

- [X] use standardized layout

- [X] use on left a sign _(positive or negative)_ and a decimal dot _(.)_

- [X] each digit will be 2 LEDS wide and have his own decimal dot too

So the display will be able to display from **-.99** to **+99** and any other values between that like **+2.5** or **-1.6**

# Connectors 

Not decided yet, but I may choose one of those solutions : 

- direct access pins

- dedicated chip compatible with 7 segment driver & a protocol like ISP 

# How to drive neopixels 

```c
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
#define PIN        6 // for one segment
#define PIN        7 // for the second one
#define NUMPIXELS 14 // for 7 segments ( 2 per segments ) 
// add all needed vars for sign &  dots  here 

Adafruit_NeoPixel pixels_segA(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixels_segB(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
#define DELAYVAL 500
// this is only for example testing on NEO_KZ800 neopixel bars

void setup() {
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  pixels_segA.begin();
  pixels_segB.begin();
}

void loop() {
  pixels_segA.clear();
  for(int i=0; i<NUMPIXELS; i++) {
    pixels_segA.setPixelColor(i, pixels_segA.Color(0, 150, 0));
    pixels_segB.setPixelColor(i, pixels_segB.Color(150, 0, 0)); // to have another color on B segments
    pixels_segA.show();
    pixels_segB.show();
    delay(DELAYVAL);
  }
}
```

Here is a good example to start playing with neopixel and understand how to drive them.

So You can also play with colors inside each digits as each DIGIT will be a serial band of 7 neopixels _(2x7 in facts)_.

See [Doc datasheet of pixel chosen](./doc/WS2812BDigiKey.pdf)


# TODO 

- [X] Draw schematic

- [X] test solution on a NEO_KHZ800 board 

- [ ] write a full working code to test the future PCB

- [X] Assign footprints

- [X] Draw PCB

- [X] Generate drill files &  gerber files

- [ ] Document the work 

## NOTES

[panel](.doc/diy_7seg_RGBpanel.pdf)

[PCB](./doc/PCB.pdf)

[SCHEMA](./doc/schema.pdf)

[NEOPIXEL](./doc/WS2812BDigiKey.pdf)
